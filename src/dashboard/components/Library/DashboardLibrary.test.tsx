import React from 'react'
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import DashboardLibrary from './DashboardLibrary'

import { Provider } from "react-redux";
import store from "../../state/store";

configure({ adapter: new Adapter() });


describe("Import dashboard from file", () => {
    let selectedDashboard = { selectedId: null, selectedIds: [ '1' ], widgets: { '1': { id: '1', x: 19, y: 8, canvas: '0', width: 10, height: 2, type: 'ATTRIBUTE_DISPLAY', inputs: { attribute: { device: 'sys/tg_test/1', attribute: 'state', label: 'State' }, precision: 2, showDevice: false, showAttribute: 'Label', scientificNotation: false, showEnumLabels: false, }, valid: true, order: 0 } }, id: '604b5fac8755940011efeea1', name: 'mydashboard', user: 'user1', group: null, groupWriteAccess: false, lastUpdatedBy: 'user1', insertTime: '2021-03-12T12:33:48.981Z', variables: [] };

    it("button renders correctly", () => {

        const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: true, 
            render: true, 
            dashboards: [], 
            selectedDashboard: []
          });
        
          const pageContent = mount(<Provider store={store}>{elementHtml}</Provider>).html();
          expect(pageContent).toContain("dashboard-menu-button");
          expect(pageContent).toContain("Add Dashboard");
          expect(pageContent).toContain("dashboard-settings-title");

          expect(pageContent).toContain("My Dashboards");
          expect(pageContent).toContain("Shared Dashboards");
          expect(pageContent).toContain("Create a new dashboard");
          expect(pageContent).toContain("Import an existing dashboard from a file");
    });

    it("hide button for not logged user", () => {

        const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: false, 
            render: true, 
            dashboards: [], 
            selectedDashboard: []
          });
        
        expect(mount(<Provider store={store}>{elementHtml}</Provider>).html()).not.toContain("dashboard-menu-button");
        expect(mount(<Provider store={store}>{elementHtml}</Provider>).html()).toContain("You have to be logged in to view and manage your dashboards.");
        
    });

    it("Test config modal", () => {

        const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: true,
            render: true,
            dashboards: [{"id":"6045fdf77a53fe001155cf8b","name":"Untitled dashboard","user":"user1","insertTime":"2021-03-08T10:35:35.958Z","updateTime":"2021-03-11T15:01:15.205Z","group":null,"lastUpdatedBy":"user1","tangoDB":"testdb"}],
            selectedDashboard: []
        });

        const dashComponent = mount(<Provider store={store}>{elementHtml}</Provider>);
        const mountedComponent = mount(elementHtml);

        expect(dashComponent.html()).toContain("Configure");
        expect(dashComponent.html()).toContain("fa-cog");

        mountedComponent.find('#dashConfig').simulate('click');
        expect(mountedComponent.state()['configDashboardModalId']).not.toBe('');
    });

    it("Test config modal fns", () => {
        let dashboardVariables = [
            {id: '11', 'name': 'CSPVar1', class: 'tg_test', device: 'sys/tg_test/1' },
            {id: '12', 'name': 'TMCVar2', class: 'webjivetestdevice', device: 'dserver/databaseds/2'}
        ];
        let filterDashboardVariables = jest.fn(),
            addDashboardVariable = jest.fn(),
            deleteDashboardVariable = jest.fn(),
            saveDashboard = jest.fn();

            const elementHtml = React.createElement(DashboardLibrary.WrappedComponent, {
            isLoggedIn: true,
            render: true,
            dashboards: [{"id":"6045fdf77a53fe001155cf8b","name":"Untitled dashboard","user":"user1","insertTime":"2021-03-08T10:35:35.958Z","updateTime":"2021-03-11T15:01:15.205Z","group":null,"lastUpdatedBy":"user1","tangoDB":"testdb", variables: dashboardVariables }],
            selectedDashboard: selectedDashboard,
            dashboardVariables: dashboardVariables,
            tangoClass: ['tg_test', 'webjivetestdevice', 'webjivetestdevice2'],
            tangoDevices: ['sys/tg_test/1', 'dserver/tangotest/test', 'dserver/databaseds/2'],
            originalDashboardVariables: dashboardVariables,
            filterDashboardVariables: filterDashboardVariables,
            deleteDashboardVariable: deleteDashboardVariable,
            addDashboardVariable: addDashboardVariable,
            saveDashboard: saveDashboard
        });

        const shallowElement = shallow(elementHtml);
        shallowElement.setState({
            configDashboardModalId: "6045fdf77a53fe001155cf8b",
            dashboardVariables: dashboardVariables,
            tangoClass: ['tg_test', 'webjivetestdevice', 'webjivetestdevice2'],
            tangoDevices: ['sys/tg_test/1', 'dserver/tangotest/test', 'dserver/databaseds/2'],
            originalDashboardVariables: dashboardVariables,
        });

        const dashComponent = mount(<Provider store={store}>{elementHtml}</Provider>);
        const mountedComponent = mount(elementHtml);

        mountedComponent.find('#dashConfig').simulate('click');
        expect(mountedComponent.state()['configDashboardModalId']).not.toBe('');

        //create new variable
        mountedComponent.find('.config-wrapper').find('.add-variable-form').simulate('click');
        mountedComponent.find('.config-wrapper').find('#add-new-var-name').simulate('change', { target: { value: 'CSP5' } });
        mountedComponent.find('.config-wrapper').find('#add-new-var-class').simulate('change', { target: { value: 'tg_test' } });
        mountedComponent.find('.config-wrapper').find('#add-new-var-device').simulate('change', { target: { value: 'sys/tg_test/1' } });

        mountedComponent.find('.config-wrapper').find('#btn-add-variable').simulate('click');
        mountedComponent.find('#config-search-variable').simulate('change', { target: { value: 'CSPVar1' } });

        mountedComponent.find('.config-wrapper').find('.btn-delete').first().simulate('click');

        expect(mountedComponent.state()['dashboardVariables'].length).toEqual(2);
        expect(mountedComponent.find('.config-wrapper').html()).toContain("Are you sure to delete");
        mountedComponent.find('.config-wrapper').find('.confirm-delete').simulate('click');
        expect(mountedComponent.state()['dashboardVariables'].length).toEqual(1);
    });
});
