import React from "react";
import Timeline from "./Timeline";

import { AttributeInput } from "../../types";
import { Inputs } from "./index";
import {
  NumberInputDefinition,
  ComplexInputDefinition,
} from "../../../dashboard/types";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Plot from "./Plot";

configure({ adapter: new Adapter() });

describe("Testing Timeline", () => {
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  let myAttributeInput: AttributeInput = {
    device: "sys/tg_test/1",
    attribute: "short_scalar",
    label: "ShortScalar",
    history: [],
    dataType: "",
    dataFormat: "",
    isNumeric: true,
    unit: "",
    enumlabels: [],
    write: writeArray,
    value: "",
    writeValue: "",
    timestamp: timestamp,
  };

  let complexInput: ComplexInputDefinition[] = [
    {
      attribute: myAttributeInput,
      showAttribute: "Label",
      yAxis: "left",
    },
  ];

  let timewindow: NumberInputDefinition = {
    type: "number",
    default: 120,
    label: "Time Window",
  };

  let myInput: Inputs = {
    timeWindow: timewindow,
    attributes: complexInput,
  };

  it("render widget with normal scenario", async () => {
    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    //check output for edit mode
    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });

  it("render widget with custom CSS", () => {
    const element = React.createElement(Timeline, {
      mode: "library",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);

    const element1 = React.createElement(Timeline, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    shallow(element1);
  });

  let plotParams = {
    height: 120,
    width: 120,
    staticMode: true,
    timeWindow: 120,
  };

  let trace = {
    x: [1, 2, 4],
    y: [2, 4, 5],
    fullName: "Timeline",
    axisLocation: "left",
  };

  let plotProps = {
    traces: [trace],
    params: plotParams,
  };

  it("Testing Plot", () => {
    let shallowElement = mount(
      <Plot traces={plotProps.traces} params={plotProps.params} />
    );
    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, "");
    expect(elemNoWhiteSpace).toContain("width:100%");
  });

  it("render widget with showAttribute name empty attribute", async () => {
    myAttributeInput = {
      device: "",
      attribute: "",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "",
      writeValue: "",
      timestamp: timestamp,
    };

    complexInput = [
      {
        attribute: myAttributeInput,
        showAttribute: "Name",
        yAxis: "left",
      },
    ];

    timewindow = {
      type: "number",
      default: 20,
      label: "Time Window",
    };

    myInput = {
      timeWindow: timewindow,
      attributes: complexInput,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });

  it("render widget with showAttribute Label empty attribute", async () => {
    myAttributeInput = {
      device: "",
      attribute: "",
      label: "",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "",
      writeValue: "",
      timestamp: timestamp,
    };

    complexInput = [
      {
        attribute: myAttributeInput,
        showAttribute: "Label",
        yAxis: "left",
      },
    ];

    timewindow = {
      type: "number",
      default: 20,
      label: "Time Window",
    };

    myInput = {
      timeWindow: timewindow,
      attributes: complexInput,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const shallowElement = shallow(element);
    expect(shallowElement.find(".timeline-wrapper").length).toEqual(1);
  });

  it("render widget with showAttribute Label", async () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "",
      writeValue: "",
      timestamp: timestamp,
    };

    complexInput = [
      {
        attribute: myAttributeInput,
        showAttribute: "Label",
        yAxis: "left",
      },
    ];

    timewindow = {
      type: "number",
      default: 20,
      label: "Time Window",
    };

    myInput = {
      timeWindow: timewindow,
      attributes: complexInput,
    };

    const element = React.createElement(Timeline, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    const mountElement = mount(element);
    mountElement
      .find(".options")
      .find("button")
      .at(0)
      .simulate("click");
    expect(mountElement.html()).toContain("Continue data update");
    mountElement
      .find(".options")
      .find("button")
      .at(0)
      .simulate("click");
    expect(mountElement.html()).toContain("Pause data update");
    mountElement
      .find(".options")
      .find("button")
      .at(1)
      .simulate("click");
    expect(mountElement.html()).not.toContain("disabled");
  });
});
