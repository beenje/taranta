import React from "react";
import { AttributeInput } from "../types";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AttributeDisplay from "./AttributeDisplay";

interface Input {
  showDevice: boolean;
  showAttribute: string;
  scientificNotation: boolean;
  precision: number;
  showEnumLables: boolean;
  attribute: AttributeInput;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  showEnumLabels: boolean;
}

configure({ adapter: new Adapter() });

describe("AttributeDisplayTests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders all false without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: "",
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "",
      scientificNotation: false,
      precision: 2,
      showEnumLables: false,
      attribute: myAttributeInput,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("AttributeDisplay");
  });

  it("renders all true without crashing", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "ShortScalar",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 123,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Name",
      scientificNotation: false,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    let element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });

    expect(shallow(element).html()).toContain("AttributeDisplay");

    //render the value with the scientific notation
    myInput = {
      showDevice: true,
      showAttribute: "Name",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

     element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });

    expect(shallow(element).html()).toContain("1.23e+2");
  });

  it("renders in edit mode before device and attribute are set", () => {
    // @ts-ignore: Deliberately set up for test without device or attribute
    myAttributeInput = {
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: 123.22,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Name",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };
    const element = React.createElement(AttributeDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("AttributeDisplay");
  });

  it("does not display the device name if showDevice is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "Name",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1");
  });

  it("does not display the device attribute if showAttribute is not set", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [],
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("DoubleSpectrum");
  });

  it("handles an undefined value", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: undefined,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Label",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1/DoubleSpectrum");
  });

  it("handles an null value", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: null,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Label",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1/DoubleSpectrum");
  });

  it("shows attribute name instead of label when showAttribute is set to Name", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: null,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Name",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).toContain("sys/tg_test/1/double_spectrum");
  });

  it("does not show any name or label on the widget", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "double_spectrum",
      label: "DoubleSpectrum",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: null,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("double_spectrum");
  });

  it("widget with Json value", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "json_display",
      label: "Json Display",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: '{"key1":1,"key2":"value"}',
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: true,
      showAttribute: "Label",
      scientificNotation: true,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: false,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });

    expect(shallow(element).html()).toContain("Json Display");
    expect(shallow(element).html()).toContain("<ul");
    
  });

  it("widget with Enum label display", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "json_display",
      label: "Json Display",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: ["On","Off"],
      write: writeArray,
      value: 0,
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "Label",
      scientificNotation: false,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: true,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1/Json");
    expect(shallow(element).html()).toContain("On");
  });

  it("widget with Json value without device display", () => {
    myAttributeInput = {
      device: "sys/tg_test/1",
      attribute: "json_display",
      label: "Json Display",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: ["On","Off"],
      write: writeArray,
      value: '{"key1":1,"key2":"value"}',
      writeValue: "",
      timestamp: timestamp
    };

    myInput = {
      showDevice: false,
      showAttribute: "",
      scientificNotation: false,
      precision: 2,
      showEnumLables: true,
      attribute: myAttributeInput,
      textColor: "black",
      backgroundColor: "white",
      size: 1,
      font: "Helvetica",
      showEnumLabels: true,
    };

    const element = React.createElement(AttributeDisplay.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput
    });
    expect(shallow(element).html()).not.toContain("sys/tg_test/1/Json");
  });
});
