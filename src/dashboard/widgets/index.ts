import attributeDisplay from "./AttributeDisplay";
import attributeWriter from "./AttributeWriter";
import attributePlot from "./AttributePlot";
import attributeScatter from "./AttributeScatter";
import label from "./Label";
import spectrum from "./Spectrum";
import spectrum2d from "./Spectrum2D";
import embedPage from "./EmbedPage";
import commandExecutor from "./CommandExecutor";
import commandWriter from "./CommandWriter";
import attributeDial from "./AttributeDial";
import booleanDisplay from "./BooleanDisplay";
import ledDisplay from "./AttributeLEDDisplay";
import sardanaMotor from "./SardanaMotor/SardanaMotor";
import attributeLogger from "./AttributeLogger/AttributeLogger";
import spectrumTable from "./SpectrumTable";
import attributeHeatMap from "./AttributeHeatMap";
import multipleCommandsExecutor from "./MultipleCommandsExecutor";
import { Widget, WidgetDefinition, WidgetBundle } from "../types";
import DeviceStatus from "./DeviceStatus";
import CommandSwitch from "./CommandSwitch";
import CommandArray from "./CommandArray";
import CommandFile from './CommandFile';
import config from "../../config.json";

import attributeWriterDropdown from "./AttributeWriterDropDown";
import Timeline from "./Timeline";
import Parametric from "./Parametric";

const widgetsToHide:Array<String> = config.WIDGETS_TO_HIDE;

let bundleData: WidgetBundle<{}>[] = [
  label,
  attributeDisplay,
  attributeWriter,
  attributeWriterDropdown,
  Parametric,
  attributePlot,
  attributeScatter,
  spectrum,
  spectrum2d,
  embedPage,
  Timeline,
  commandExecutor,
  commandWriter,
  CommandArray,
  attributeDial,
  CommandFile,
  CommandSwitch,
  booleanDisplay,
  DeviceStatus,
  ledDisplay,
  sardanaMotor,
  attributeLogger,
  attributeHeatMap,
  spectrumTable,
  multipleCommandsExecutor,
];

if (Array.isArray(widgetsToHide) && widgetsToHide.length > 0) {
  bundleData = bundleData.filter((bundle) => (-1 === widgetsToHide.indexOf(bundle.definition.type)));
}
export const bundles = bundleData;

function bundleForType(type: string) {
  const bundle = bundles.find(bundle => bundle.definition.type === type);

  if (bundle == null) {
    throw new Error(`No bundle for type ${type}`);
  }

  return bundle;
}

export function definitionForType(type: string): WidgetDefinition<{}> {
  const bundle = bundleForType(type);
  return bundle.definition;
}

export function componentForType(type: string) {
  const bundle = bundleForType(type);
  return bundle.component;
}

export function bundleForWidget(widget: Widget) {
  return bundleForType(widget.type);
}

export function definitionForWidget(widget: Widget) {
  return definitionForType(widget.type);
}

export function componentForWidget(widget: Widget) {
  return componentForType(widget.type);
}