import { WidgetProps } from "./types";
import {parseCss} from "../components/Inspector/StyleSelector"
import React, { Component, Fragment, CSSProperties, ChangeEvent } from "react";
import { WidgetDefinition, CommandInputDefinition, StringInputDefinition, StyleInputDefinition, BooleanInputDefinition, NumberInputDefinition } from "../types";

import "./styles/BooleanDisplay.styles.css";

type Inputs = {
  title: StringInputDefinition
  onCommand: CommandInputDefinition;
  offCommand: CommandInputDefinition;
  showDevice: BooleanInputDefinition;
  showCommand: BooleanInputDefinition
  onStateArgs: StringInputDefinition;
  offStateArgs: StringInputDefinition;
  onStateImageUrl: StringInputDefinition;
  offStateImageUrl: StringInputDefinition;
  onStateCss: StyleInputDefinition;
  offStateCss: StyleInputDefinition;
  imageCss: StyleInputDefinition;
  displayOutput: BooleanInputDefinition;
  timeDisplayOutput: NumberInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  checkbox: boolean;
  pending: boolean;
  title: string;
  onStateArgs: string;
  offStateArgs: string;
  showDevice: boolean;
  showCommand: boolean;
  onStateImageUrl: string;
  offStateImageUrl: string;
  onStateCss: any,
  offStateCss: any,
  imageCss: any
}

const style = { padding: "0.5em", whiteSpace: "nowrap" } as CSSProperties;
const styleCheckbox = { padding: "0.5em" } as CSSProperties;
const imageStyle = {
  width: "50px",
  height: "25px"
}
let outerDivCss;
let waiting = false,displayOutputCommand = "";

class CommandSwitch extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      pending: false,
      checkbox: false,
      title: this.getAttributeValue('title'),
      showDevice: this.getAttributeValue('showDevice'),
      showCommand: this.getAttributeValue('showCommand'),
      onStateArgs: this.getAttributeValue('onStateArgs'),
      offStateArgs: this.getAttributeValue('offStateArgs'),
      onStateImageUrl: this.getAttributeValue('onStateImageUrl'),
      offStateImageUrl: this.getAttributeValue('offStateImageUrl'),
      onStateCss: parseCss(this.getAttributeValue('onStateCss')).data,
      offStateCss: parseCss(this.getAttributeValue('offStateCss')).data,
      imageCss: parseCss(this.getAttributeValue('imageCss')).data,
    };

    this.checkboxChange = this.checkboxChange.bind(this);
  }

  public render() {
    const { title,showDevice, showCommand, timeDisplayOutput } = this.props.inputs;
    const onCommandName = this.props.inputs.onCommand.command;
    const offCommandName = this.props.inputs.offCommand.command;

    const [output, outputStyle] = this.outputAndStyle();

    if(output[0]!==undefined && (displayOutputCommand === onCommandName ||
      displayOutputCommand ===  offCommandName) && !waiting) 
    {
      waiting = true;
      setTimeout(()=> {
        displayOutputCommand = "";
        waiting = false;
        this.setState({});
      }, timeDisplayOutput);
    }

    const fullOutputStyle: CSSProperties = {
      marginLeft: "0.5em",
      fontSize: "80%",
      ...outputStyle
    };
    const style: CSSProperties = {
      display: "flex",
          alignItems: "center",
          padding: "0.25em 0.5em"
    }

    outerDivCss = this.state.checkbox === true ? {...style, ...this.state.onStateCss} : {...style, ...this.state.offStateCss};

    let titleLabel = undefined !== title && '' !== title ? title+' ' : '';
    titleLabel += showDevice ? this.showSelectedDeviceOrCommand('device') : '';
    titleLabel += showCommand ? this.showSelectedDeviceOrCommand('command') : '';

    let Checkbox;
    if (false === this.validateUrl(this.state.onStateImageUrl) || false === this.validateUrl(this.state.offStateImageUrl)) {
        Checkbox =
          (
            <span style={styleCheckbox}>
            <label className="toggle">
                <input
                className="attribute-checkbox"
                type="checkbox"
                checked={this.state.checkbox}
                onChange={this.checkboxChange}
                />
                <span className="slider round"></span>
            </label>
            </span>
          );
    } else {
      const imageUrl = true === this.state.checkbox ? this.state.onStateImageUrl : this.state.offStateImageUrl;
      const imageCss = undefined !== this.state.imageCss ? {...imageStyle, ...this.state.imageCss} : imageStyle;

      Checkbox =
        (
          <span style={styleCheckbox}>
          <label className="toggle">
              <input
                  type="checkbox"
                  className="attribute-checkbox"
                  id="attribute-checkbox"
                  checked={this.state.checkbox}
                  onChange={this.checkboxChange}
                  />
              <img alt="not found" src={imageUrl} style={imageCss} />

          </label>
          </span>
      );
    }


    let outputSpan = displayOutputCommand === onCommandName ||  displayOutputCommand === offCommandName ? 
    <span style={fullOutputStyle}>{output}</span> : null;

    return (
      <div style={outerDivCss}>
        <Fragment>
          {titleLabel} {Checkbox}
          {outputSpan}
        </Fragment>
      </div>
    );
  }

  private getAttributeValue(key): any {

    if(key === 'onStateCss') {
        return this.props.inputs.onStateCss;
    } else if(key === 'offStateCss') {
        return this.props.inputs.offStateCss;
    } else if(key === 'onStateImageUrl') {
        return this.props.inputs.onStateImageUrl;
    } else if(key === 'offStateImageUrl') {
        return this.props.inputs.offStateImageUrl;
    } else if(key === 'onStateArgs') {
        return this.props.inputs.onStateArgs || 'one';
    } else if(key === 'offStateArgs') {
        return this.props.inputs.offStateArgs || 'zero';
    } else if(key === 'imageCss') {
        return this.props.inputs.imageCss;
    } else if(key === 'title') {
        return this.props.inputs.title;
    } else if(key === 'showDevice') {
        return this.props.inputs.showDevice;
    } else if(key === 'showCommand') {
        return this.props.inputs.showCommand;
    }
  }

  /**
   * Returns Device / Command value depending on keyword. If Device & Command are null returns "Device" & "Command"
   *
   * @param keyword: String
   */
  showSelectedDeviceOrCommand(keyword) {
    const { onCommand, offCommand } = this.props.inputs;
    let returnVal = '';

    if ('device' === keyword) {
      returnVal = this.state.checkbox ? null !== onCommand.device ? onCommand.device+'/' : 'Device/' : null !== offCommand.device ? offCommand.device+'/' : 'Device/';
    } else {
      returnVal = this.state.checkbox ? null !== onCommand.command ? onCommand.command : 'Command' : null !== offCommand.command ? offCommand.command : 'Command';
    }

    return returnVal;
  }

  private async checkboxChange(event: ChangeEvent<HTMLInputElement>) {
    if (true === this.state.pending) {
      return;
    }

    const isChecked = this.state.checkbox;
    const { onCommand, offCommand } = this.props.inputs;
    outerDivCss = this.state.checkbox === true ? {...style, ...this.state.onStateCss} : {...style, ...this.state.offStateCss};
    this.setState({ checkbox: !isChecked, pending: true });

    if (false === this.state.checkbox) {
      const acceptedType: string = this.props.inputs.onCommand["acceptedType"];
      const input = this.props.inputs.onStateArgs;

      let result = this.inputConvertion(input,acceptedType);
      displayOutputCommand = onCommand.command;
      onCommand.execute(result);

    } else {
      const acceptedType: string = this.props.inputs.offCommand["acceptedType"];
      const input = this.props.inputs.offStateArgs;

      let result = this.inputConvertion(input,acceptedType);
      displayOutputCommand = offCommand.command;
      offCommand.execute(result);
    }

    this.setState({ pending: false });
  }

  private isInteger(value) {
    return /^\d+$/.test(value);
  }

  private isFloat(value) {
    return !Number.isNaN(Number.parseFloat(value));
  }

  private isArray(value) {
    return value[0] === '[';
  }

  private isObject(value) {
    return value[0] === '{';
  }

  private isString(value) {
    return value[0] === '"';
  }

  private validateInputArray(input, acceptedType: string){
    if(input.indexOf(',') !== -1)
    {
      let data = input.split(","); 
  
      if(acceptedType.includes("String")){
        let list: Array<string> = [];
        data.forEach(element => {
          list.push(element);
        })
        return list;
      }
      else if (acceptedType.includes("Char")){
        let list: Array<number> = [];
        data.forEach(element => {
          list.push(Number.parseInt(element[1]));
        })
        return list;
      }
      else{
        let list: Array<Object> = [];
        data.forEach(element => {
          list.push(Number.parseFloat(element));
        })
        return list;
      }
    }
    else {
      if(acceptedType.includes("String")){
        let list: Array<string> = [];
        list.push(input);
        return list;
      }
      else 
      {
        let list: Array<Object> = [];
        list.push(Number.parseFloat(input));
        return list;
      }
     
    }
  }

  private inputConvertion(input,acceptedType){
    let result;
    if(!input) result = ""; 
    else if(input === "True" || input === "true") result = true;
    else if(input === "False" || input === "false") result = false;
    else if(this.isInteger(input)) result = parseInt(input); 
    else if(this.isFloat(input)) result = parseFloat(input); 
    else if(this.isArray(input))
    {
      let inputArray: Array<Object>;
      var trim = input.substring(1, input.length-1);
      acceptedType.includes("Array") ?  inputArray = this.validateInputArray(trim, acceptedType) : inputArray = [];
      result = inputArray;
    }
    else if(this.isObject(input)) result = input; 
    else if(this.isString(input))
    {
      trim = input.substring(1, input.length-1);
      result = trim;
    }
    //else are there other possible types?

    return result;
  }

  private validateUrl(url) {
    return (undefined !== url && -1 !== url.indexOf('http'));
  }

  private outputAndStyle(): [string, CSSProperties] {
    const { mode, inputs } = this.props;
    const { onCommand, displayOutput} = inputs;
    const { output } = onCommand;

    const shownOutput = displayOutput
      ? mode === "run"
        ? output
        : "output"
      : "";

    if (mode !== "run") {
      return [shownOutput, { color: "gray", fontStyle: "italic" }];
    }

    if (displayOutput && !output) {
      return ["n/a", { color: "gray" }];
    }
    
    return [shownOutput, {}];
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "Command_Switch",
  name: "Command Switch",
  defaultWidth: 12,
  defaultHeight: 2,

  inputs: {
    title: {
      type:"string",
      label: "Title",
      placeholder: "Title of widget"
    },
    onCommand: {
      label: "On Switch Command",
      type: "command",
      intype: "Any"
    },
    offCommand: {
      label: "Off Switch Command",
      type: "command",
      intype: "Any"
    },
    showDevice: {
      type: "boolean",
      label: "Show Device",
      default: true
    },
    showCommand: {
      type: "boolean",
      label: "Show Command",
      default: true
    },
    displayOutput: {
      type: "boolean",
      label: "Display Output",
      default: true
    },
    timeDisplayOutput: {
      type: "number",
      label: "TimeOut for output display ms",
      default: 3000,
      nonNegative: true,
    },
    onStateArgs: {
        type: "string",
        label: "On State Argument",
        placeholder: "Ex. [1,2,3]"
    },
    offStateArgs: {
        type: "string",
        label: "Off State Argument",
        placeholder: "Ex. [1,2,3]"
    },
    onStateImageUrl: {
        type: "string",
        label: "On State Image Url",
        placeholder: "https://imagelocation?onImage.jpg"
    },
    offStateImageUrl: {
        type: "string",
        label: "Off State Image Url",
        placeholder: "https://imagelocation?offImage.jpg"
    },
    onStateCss: {
        type: "style",
        label: "On State CSS"
    },
    offStateCss: {
        type: "style",
        label: "Off State CSS"
    },
    imageCss: {
      type: "style",
      label: "Image CSS"
    }
  }
};

export default { component: CommandSwitch, definition };
