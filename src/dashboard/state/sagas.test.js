import { call } from 'redux-saga/effects';
import {
    saveDashboard,
    loadDashboardSaga,
    deleteDashboard,
    cloneDashboard,
    renameDashboard,
    shareDashboard,
    loadDashboards,
    exportDashboardSaga,
    editWidget,
    importDashboardSaga,
    notifyOnSave,
    notifyOnShare,
    notifyOnClone,
    notifyOnDelete,
    hideNotificationAfterDelay,
    notifyOnExport,
} from './sagas';
import {
    dashboardSaved,
    saveDashboard as saveDashboardAction,
    dashboardDeleted,
    dashboardCloned,
    dashboardRenamed,
    dashboardShared,
} from "./actionCreators";
import {
    SAVE_DASHBOARD,
    DASHBOARD_SAVED,
    LOAD_DASHBOARD,
    DASHBOARD_LOADED,
    DELETE_DASHBOARD,
    DASHBOARD_DELETED,
    CLONE_DASHBOARD,
    DASHBOARD_CLONED,
    RENAME_DASHBOARD,
    DASHBOARD_RENAMED,
    DASHBOARD_SHARED,
    SHARE_DASHBOARD,
    DASHBOARDS_LOADED,
    EXPORT_DASHBOARD,
    DASHBOARD_EXPORTED,
    DASHBOARD_EDITED,
    ADD_WIDGET,
    MOVE_WIDGETS,
    RESIZE_WIDGET,
    DELETE_WIDGET,
    UNDO,
    REDO,
    DUPLICATE_WIDGET,
    SET_INPUT,
    DELETE_INPUT,
    REORDER_WIDGETS,
    ADD_INPUT,
    WIDGET_CLIPBOARD_PASTE,
    IMPORT_DASHBOARD,
    DASHBOARD_CREATED,
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION,
} from "./actionTypes";

import * as API from "../dashboardRepo";

const exampleWidget = {
    "id": "1",
    "x": 28,
    "y": 12,
    "canvas": "0",
    "width": 10,
    "height": 3,
    "type": "ATTRIBUTE_DISPLAY",
    "inputs": {
        "attribute": {
            "device": "test/webjivetestdevice/1",
            "attribute": "cbfobsstate",
            "label": "CbfObsState"
        },
        "precision": 2,
        "showDevice": false,
        "showAttribute": "Label",
        "scientificNotation": false,
        "showEnumLabels": false,
        "textColor": "#000000",
        "backgroundColor": "#ffffff",
        "size": 1,
        "font": "Helvetica"
    },
    "order": 0,
    "valid": true
};

const DASHEDITEXAMPLE = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "6092ba6bd758430012abd938",
            "id": "1",
            "x": 29,
            "y": 9,
            "canvas": "0",
            "width": 10,
            "height": 3,
            "type": "ATTRIBUTE_DISPLAY",
            "inputs": {
                "attribute": {
                    "device": "test/webjivetestdevice/1",
                    "attribute": "state",
                    "label": "State"
                },
                "precision": 2,
                "showDevice": false,
                "showAttribute": "Label",
                "scientificNotation": false,
                "showEnumLabels": false,
                "textColor": "#000000",
                "backgroundColor": "#ffffff",
                "size": 1,
                "font": "Helvetica"
            },
            "order": 0,
            "valid": true
        }
    },
    "variables": [],
    "id": "6092b53fd758430012abd91e",
    "name": "parametric1",
    "user": "CREAM",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "CREAM",
    "insertTime": "2021-05-05T15:09:51.032Z",
    "updateTime": "2021-05-07T09:39:55.986Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "6092ba6bd758430012abd938",
                    "id": "1",
                    "x": 30,
                    "y": 9,
                    "canvas": "0",
                    "width": 10,
                    "height": 3,
                    "type": "ATTRIBUTE_DISPLAY",
                    "inputs": {
                        "attribute": {
                            "device": "test/webjivetestdevice/1",
                            "attribute": "state",
                            "label": "State"
                        },
                        "precision": 2,
                        "showDevice": false,
                        "showAttribute": "Label",
                        "scientificNotation": false,
                        "showEnumLabels": false,
                        "textColor": "#000000",
                        "backgroundColor": "#ffffff",
                        "size": 1,
                        "font": "Helvetica"
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 1,
        "redoIndex": 0,
        "undoLength": 1,
        "redoLength": 0
    }
};

const ADD_DASHEDITEXAMPLE = {
    "selectedId": null,
    "selectedIds": [
        "1"
    ],
    "widgets": {
        "1": {
            "_id": "60a2301df41d600011f0869b",
            "id": "1",
            "x": 14,
            "y": 5,
            "canvas": "0",
            "width": 30,
            "height": 20,
            "type": "TIMELINE",
            "inputs": {
                "timeWindow": 120,
                "overflow": false,
                "groupAttributes": false,
                "attributes": [
                    {
                        "attribute": {
                            "device": "sys/tg_test/1",
                            "attribute": "state",
                            "label": "State"
                        },
                        "yAxisDisplay": "Label",
                        "showAttribute": "Label",
                        "yAxis": "left"
                    },
                    {
                        "attribute": {
                            "device": "dserver/databaseds/2",
                            "attribute": "state",
                            "label": "State"
                        },
                        "yAxisDisplay": "Label",
                        "showAttribute": "Label",
                        "yAxis": "left"
                    }
                ]
            },
            "order": 0,
            "valid": true
        }
    },
    "id": "604b5fac8755940011efeea1",
    "name": "mydashboard",
    "user": "user1",
    "group": null,
    "groupWriteAccess": false,
    "lastUpdatedBy": "user1",
    "insertTime": "2021-03-12T12:33:48.981Z",
    "updateTime": "2021-05-17T08:58:05.031Z",
    "history": {
        "undoActions": [
            {
                "1": {
                    "_id": "60a2301df41d600011f0869b",
                    "id": "1",
                    "x": 14,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "TIMELINE",
                    "inputs": {
                        "timeWindow": 120,
                        "overflow": false,
                        "groupAttributes": false,
                        "attributes": [
                            {
                                "attribute": {
                                    "device": null,
                                    "attribute": null
                                },
                                "yAxisDisplay": "Label",
                                "showAttribute": "Label",
                                "yAxis": "left"
                            }
                        ]
                    },
                    "order": 0,
                    "valid": false
                }
            },
            {
                "1": {
                    "_id": "60a2301df41d600011f0869b",
                    "id": "1",
                    "x": 14,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "TIMELINE",
                    "inputs": {
                        "timeWindow": 120,
                        "overflow": false,
                        "groupAttributes": false,
                        "attributes": [
                            {
                                "attribute": {
                                    "device": "sys/tg_test/1",
                                    "attribute": null,
                                    "label": null
                                },
                                "yAxisDisplay": "Label",
                                "showAttribute": "Label",
                                "yAxis": "left"
                            }
                        ]
                    },
                    "order": 0,
                    "valid": false
                }
            },
            {
                "1": {
                    "_id": "60a2301df41d600011f0869b",
                    "id": "1",
                    "x": 14,
                    "y": 5,
                    "canvas": "0",
                    "width": 30,
                    "height": 20,
                    "type": "TIMELINE",
                    "inputs": {
                        "timeWindow": 120,
                        "overflow": false,
                        "groupAttributes": false,
                        "attributes": [
                            {
                                "attribute": {
                                    "device": "sys/tg_test/1",
                                    "attribute": "state",
                                    "label": "State"
                                },
                                "yAxisDisplay": "Label",
                                "showAttribute": "Label",
                                "yAxis": "left"
                            }
                        ]
                    },
                    "order": 0,
                    "valid": true
                }
            }
        ],
        "redoActions": [],
        "undoIndex": 3,
        "redoIndex": 0,
        "undoLength": 3,
        "redoLength": 0
    },
    "variables": []
}

const editWidgetActions = {
    pattern: [
        ADD_WIDGET,
        MOVE_WIDGETS,
        RESIZE_WIDGET,
        DELETE_WIDGET,
        UNDO,
        REDO,
        DUPLICATE_WIDGET,
        SET_INPUT,
        DELETE_INPUT,
        REORDER_WIDGETS,
        ADD_INPUT,
        WIDGET_CLIPBOARD_PASTE,
    ]
}

const stepper = (fn) => (mock) => fn.next(mock).value;

test('the saveDashboard saga happy path', async () => {

    const mockSaveDash = {
        id: 123,
        widgets: [exampleWidget],
        name: 'Dash1',
        variables: [],
    };

    const mockSaveDashResponse = {
        id: 1234,
        created: true,
    };

    const step = stepper(saveDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: SAVE_DASHBOARD });

    const SageStep2call = step(mockSaveDash);
    expect(SageStep2call).toEqual(
        call(API.save, mockSaveDash.id, mockSaveDash.widgets, mockSaveDash.name, []));
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual(mockSaveDash.id);
    expect(SageStep2call.payload.args[1]).toEqual(mockSaveDash.widgets);
    expect(SageStep2call.payload.args[2]).toEqual(mockSaveDash.name);

    const saveAction = saveDashboardAction(mockSaveDash.id, mockSaveDash.name, mockSaveDash.widgets);
    expect(SageStep2call.payload.args[0]).toEqual(saveAction.id);
    expect(SageStep2call.payload.args[1]).toEqual(saveAction.widgets);
    expect(SageStep2call.payload.args[2]).toEqual(saveAction.name);

    const SageStep3put = step(mockSaveDashResponse);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        { type: DASHBOARD_SAVED, id: 1234, created: true, name: 'Dash1', variables: [] });
    expect(SageStep3put.payload.action).toEqual(dashboardSaved(1234, true, 'Dash1', []));
});

test('the saveDashboard saga sad path', async () => {

    const mockSaveDash = {
        id: 123,
        widgets: [exampleWidget],
        name: 'Dash1',
    };

    const step = stepper(saveDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: SAVE_DASHBOARD });

    const SageStep2call = step(mockSaveDash);
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual(mockSaveDash.id);
    expect(SageStep2call.payload.args[1]).toEqual(mockSaveDash.widgets);
    expect(SageStep2call.payload.args[2]).toEqual(mockSaveDash.name);

    const SageStep3put = step();// not passing necessary return info
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action.type).toEqual('SHOW_NOTIFICATION');
    expect(SageStep3put.payload.action.notification.level).toEqual('Error');
});

test('the loadDashboardSaga type LOAD_DASHBOARD happy path', async () => {

    const mockLoadDash = {
        id: 123,
        type: LOAD_DASHBOARD,
    };

    const mockLoadDashResponse = {
        id: 123,
        name: 'Dash1',
        user: 'User1',
        insertTime: '16:05',
        updateTime: '16:05',
        group: 'Group1',
        groupWriteAccess: true,
        lastUpdatedBy: 'User1',
        widgets: [],
    };

    const step = stepper(loadDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({
        pattern: [
            LOAD_DASHBOARD,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED
        ]
    });

    const SageStep2call = step(mockLoadDash);
    expect(SageStep2call).toEqual(call(API.load, mockLoadDash.id));

    const SageStep3put = step(mockLoadDashResponse);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        {
            type: DASHBOARD_LOADED,
            dashboard: {
                id: 123,
                name: 'Dash1',
                user: 'User1',
                insertTime: '16:05',
                updateTime: '16:05',
                group: 'Group1',
                groupWriteAccess: true,
                lastUpdatedBy: 'User1',
            }, widgets: [],
        });
});

test('the loadDashboardSaga type DASHBOARD_SAVED happy path', async () => {

    const mockLoadDash = {
        id: 123,
        type: DASHBOARD_SAVED,
        created: true,
    };

    const mockLoadDashResponse = {
        id: 123,
        name: 'Dash1',
        user: 'User1',
        insertTime: '16:05',
        updateTime: '16:05',
        group: 'Group1',
        groupWriteAccess: true,
        lastUpdatedBy: 'User1',
        widgets: [],
    };

    const step = stepper(loadDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({
        pattern: [
            LOAD_DASHBOARD,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED
        ]
    });

    const SageStep2call = step(mockLoadDash);
    expect(SageStep2call).toEqual(call(API.load, mockLoadDash.id));

    const SageStep3put = step(mockLoadDashResponse);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        {
            type: DASHBOARD_LOADED,
            dashboard: {
                id: 123,
                name: 'Dash1',
                user: 'User1',
                insertTime: '16:05',
                updateTime: '16:05',
                group: 'Group1',
                groupWriteAccess: true,
                lastUpdatedBy: 'User1',
            }, widgets: [],
        });
});

test('the loadDashboardSaga sad path', async () => {

    const mockLoadDash = {
        id: 123,
        type: LOAD_DASHBOARD,
    };

    const step = stepper(loadDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({
        pattern: [
            LOAD_DASHBOARD,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED
        ]
    });

    const SageStep2call = step(mockLoadDash);
    expect(SageStep2call).toEqual(call(API.load, mockLoadDash.id));

    const SageStep3put = step();// not passing necessary return info
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action.type).toEqual('SHOW_NOTIFICATION');
    expect(SageStep3put.payload.action.notification.level).toEqual('Error');
});

test('the deleteDashboard saga happy path', async () => {

    const step = stepper(deleteDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: DELETE_DASHBOARD });


    const SageStep2call = step({ id: 123 });
    expect(SageStep2call).toEqual(
        call(API.deleteDashboard, 123));
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual(123);

    const SageStep3put = step({ id: 123 });
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        { type: DASHBOARD_DELETED, id: 123 });
    expect(SageStep3put.payload.action).toEqual(dashboardDeleted(123));
});

test('the cloneDashboard saga happy path', async () => {

    const step = stepper(cloneDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: CLONE_DASHBOARD });

    const SageStep2call = step({ id: 123 });
    expect(SageStep2call).toEqual(
        call(API.cloneDashboard, 123));
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual(123);

    const SageStep3put = step({ id: 123 });
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        { type: DASHBOARD_CLONED, id: 123 });
    expect(SageStep3put.payload.action).toEqual(dashboardCloned(123));
});

test('the renameDashboard saga happy with ID path', async () => {

    const step = stepper(renameDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: RENAME_DASHBOARD });

    const SageStep2call = step({ id: '123', name: 'NewName' });
    expect(SageStep2call).toEqual(
        call(API.renameDashboard, '123', 'NewName'));
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual('123');

    const SageStep3put = step({ id: '123', name: 'NewName' });
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        { type: DASHBOARD_RENAMED, id: '123', name: 'NewName' });
    expect(SageStep3put.payload.action).toEqual(dashboardRenamed('123', 'NewName'));
});

test('the renameDashboard saga happy without ID path', async () => {

    const step = stepper(renameDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: RENAME_DASHBOARD });

    const SageStep2call = step({ id: '', name: 'NewName' });

    expect(SageStep2call.type).toEqual('SELECT');

    const SageStep3put = step([exampleWidget]);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action.type).toEqual('SAVE_DASHBOARD');
    expect(SageStep3put.payload.action.widgets[0]).toEqual(exampleWidget);
});

test('the shareDashboard saga happy path', async () => {

    const shareExample = { id: '123', group: 'Group1', groupWriteAccess: true };

    const step = stepper(shareDashboard());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: SHARE_DASHBOARD });

    const SageStep2call = step(shareExample);
    expect(SageStep2call).toEqual(
        call(API.shareDashboard, '123', 'Group1', true));
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call.payload.args[0]).toEqual('123');

    const SageStep3put = step(shareExample);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        { type: DASHBOARD_SHARED, id: '123', group: 'Group1', groupWriteAccess: true });
    expect(SageStep3put.payload.action).toEqual(dashboardShared('123', 'Group1', true));
});

test('the loadDashboards saga happy path', async () => {

    const mockDash = {
        id: 123,
        widgets: [exampleWidget],
        name: 'Dash1',
    };

    const step = stepper(loadDashboards());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({
        pattern: [
            'PRELOAD_USER_SUCCESS',
            'LOGIN_SUCCESS',
            DASHBOARD_RENAMED,
            DASHBOARD_DELETED,
            DASHBOARD_CLONED,
            DASHBOARD_SAVED,
        ]
    });

    const SageStep2call = step({ type: DASHBOARD_CLONED, created: true });
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call).toEqual(call(API.loadUserDashboards));

    const SageStep3put = step(mockDash);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual({ type: DASHBOARDS_LOADED, dashboards: mockDash });
});

test('the exportDashboardSaga happy path', async () => {

    const mockLoadDash = {
        id: 123,
        type: LOAD_DASHBOARD,
    };

    const mockLoadDashResponse = {
        id: 123,
        name: 'Dash1',
        user: 'User1',
        insertTime: '16:05',
        updateTime: '16:05',
        group: 'Group1',
        groupWriteAccess: true,
        lastUpdatedBy: 'User1',
        widgets: [],
    };

    const step = stepper(exportDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: EXPORT_DASHBOARD });

    const SageStep2call = step(mockLoadDash);
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call).toEqual(call(API.exportDash, mockLoadDash.id));


    const SageStep3put = step(mockLoadDashResponse);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        {
            type: DASHBOARD_EXPORTED,
            dashboard: {
                id: 123,
                name: 'Dash1',
                user: 'User1',
                insertTime: '16:05',
                updateTime: '16:05',
                group: 'Group1',
                groupWriteAccess: true,
                lastUpdatedBy: 'User1',
            }, widgets: [],
        });
});

test('the importDashboardSaga happy path with warning', async () => {
    const mydash = {
        "id": "6149a2755c96d300129af1e8",
        "name": "upload111",
        "version": "1.1.4",
        "user": "user1",
        "insertTime": "2021-09-21T09:14:29.315Z",
        "updateTime": "2021-10-01T07:19:43.991Z",
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "user1",
        "widget": [
          {
            "id": "1",
            "x": 11,
            "y": 2,
            "canvas": "0",
            "width": 23,
            "height": 19,
            "type": "TIMELINE",
            "inputs": {
              "timeWindow": 120,
              "overflow": false,
              "groupAttributes": false,
              "attributes": [
                {
                  "attribute": {
                    "device": "test/webjivetestdevice/1",
                    "attribute": "cbfobsstate",
                    "label": "CbfObsState"
                  },
                  "yAxisDisplay": "Label",
                  "showAttribute": "Label",
                  "yAxis": "left"
                }
              ]
            },
            "order": 0
          },
          {
            "id": "2",
            "x": 36,
            "y": 5,
            "canvas": "0",
            "width": 10,
            "height": 2,
            "type": "ATTRIBUTE_DISPLAYYY",
            "inputs": {
              "attribute": {
                "device": "sys/tg_test/1",
                "attribute": "double_scalar",
                "label": "double_scalar"
              },
              "precision": 2,
              "showDevice": false,
              "showAttribute": "Label",
              "scientificNotation": false,
              "showEnumLabels": false,
              "textColor": "#000000",
              "backgroundColor": "#ffffff",
              "size": 1,
              "font": "Helvetica"
            },
            "order": 1
           }
        ]
    };
    const mockImported = {
        "id": "609933c6dc39020012f54b77",
        "created": true,
        "name": "parametric1",
        "warning": "Widget definition not found for ATTRIBUTE_DISPLAYYY"
    };

    const step = stepper(importDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: IMPORT_DASHBOARD });

    const SageStep2call = step({ content: mydash });
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call).toEqual(call(API.importDash, mydash));

    const SageStep3put = step(mockImported);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual({
        type: 'SHOW_NOTIFICATION',
        notification: {
            level: 'Warning',
            action: 'IMPORT_DASHBOARD',
            msg: 'Widget definition not found for ATTRIBUTE_DISPLAYYY',
            duration: 2000
        }
    });

    const SageStep4put = step(mockImported);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action).toEqual({
        type: 'DASHBOARD_SAVED',
        id: '609933c6dc39020012f54b77',
        created: true,
        name: 'parametric1',
        variables: undefined
    });
});

test('the importDashboardSaga happy path', async () => {

    const importDashHappyPath = {
        "id": "6149a2755c96d300129af1e8",
        "name": "upload111",
        "version": "1.1.4",
        "user": "user1",
        "insertTime": "2021-09-21T09:14:29.315Z",
        "updateTime": "2021-10-01T07:19:43.991Z",
        "group": null,
        "groupWriteAccess": false,
        "lastUpdatedBy": "user1",
        "widget": [
          {
            "id": "1",
            "x": 11,
            "y": 2,
            "canvas": "0",
            "width": 23,
            "height": 19,
            "type": "TIMELINE",
            "inputs": {
              "timeWindow": 120,
              "overflow": false,
              "groupAttributes": false,
              "attributes": [
                {
                  "attribute": {
                    "device": "test/webjivetestdevice/1",
                    "attribute": "cbfobsstate",
                    "label": "CbfObsState"
                  },
                  "yAxisDisplay": "Label",
                  "showAttribute": "Label",
                  "yAxis": "left"
                }
              ]
            },
            "order": 0
          }
        ]
      };

    const mockImported = {
        "id": "609933c6dc39020012f54b77",
        "created": true,
        "name": "parametric1",
        "warning": ""
    };

    const step = stepper(importDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: IMPORT_DASHBOARD });

    const SageStep2call = step({ content: importDashHappyPath });
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call).toEqual(call(API.importDash, importDashHappyPath));

    const SageStep4put = step(mockImported);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action).toEqual({
        type: 'DASHBOARD_SAVED',
        id: '609933c6dc39020012f54b77',
        created: true,
        name: 'parametric1',
        variables: undefined
    });
});

test('the importDashboardSaga sad path', async () => {

    const mockImportDash = {
        "id": "files-1",
        "extension": "wj",
        "sizeReadable": "891B",
        "preview": {
            "type": "file"
        }
    };

    const mockImported = {
        "id": "609933c6dc39020012f54b77",
        "name": "parametric1"
    };

    const step = stepper(importDashboardSaga());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({ pattern: IMPORT_DASHBOARD });

    const SageStep2call = step({ content: mockImportDash });
    expect(SageStep2call.type).toEqual('CALL');
    expect(SageStep2call).toEqual(call(API.importDash, mockImportDash));

    const SageStep3put = step(mockImported);
    expect(SageStep3put.type).toEqual('PUT');
    expect(SageStep3put.payload.action).toEqual(
        {
            type: 'SHOW_NOTIFICATION',
            notification: {
                level: 'Error',
                action: 'IMPORT_DASHBOARD',
                msg: 'Dashboard not imported: [object Object]',
                duration: 2000
            }
        }
    );
});

test('the editWidget MOVE_WIDGET happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: MOVE_WIDGETS,
        "dx": 0,
        "dy": 0,
        "ids": [
            "1"
        ]
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
});

test('the editWidget SET_INPUT happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: SET_INPUT,
        "path": [
            "precision"
        ],
        "value": 3,
        "widgetType": "ATTRIBUTE_DISPLAY"
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');

    const SageStep2select = step(DASHEDITEXAMPLE);
    expect(SageStep2select.type).toEqual('SELECT');

    const SageStep2put = step([DASHEDITEXAMPLE]);
    expect(SageStep2put.type).toEqual('PUT');
    expect(SageStep2put.payload.action.type).toEqual('DASHBOARD_EDITED');

    const SageStep3save = step();
    expect(SageStep3save.type).toEqual('PUT');
    expect(SageStep3save.payload.action.type).toEqual('SAVE_DASHBOARD');
    expect(SageStep3save.payload.action.id).toEqual('6092b53fd758430012abd91e');
});

test('the editWidget UNDO happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: UNDO,
    };
    
    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});

test('the editWidget REDO happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: REDO,
    };
    
    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});

test('the editWidget ADD_WIDGET happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: ADD_WIDGET,
        x: 34,
        y: 8,
        widgetType: "ATTRIBUTE_DISPLAY",
        canvas: "0"
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    
    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});

test('the editWidget ADD_INPUT happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        "type": ADD_INPUT,
        "path": [
            "attributes"
        ]
    }

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(ADD_DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([ADD_DASHEDITEXAMPLE]);
    //check if newly added input is added to attributes array
    expect(SageStep4put.payload.action.dashboard.widgets['1'].inputs.attributes.length).toEqual(3);

    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('604b5fac8755940011efeea1');
});


test('the editWidget DELETE_INPUT happy path', async () => {

    const step = stepper(editWidget());
    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        "type": "DELETE_INPUT",
        "path": [
            "attributes",
            1
        ]
    }
    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(ADD_DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([ADD_DASHEDITEXAMPLE]);
    //check if deleted attribute is removed from attributes array
    expect(SageStep4put.payload.action.dashboard.widgets['1'].inputs.attributes.length).toEqual(1);
});

test('the editWidget REORDER_WIDGETS happy path', async () => {

    const step = stepper(editWidget());
    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        "type": REORDER_WIDGETS,
        widgets: Object.values(DASHEDITEXAMPLE.widgets)
    }

    const reorderedWigets = {
        '1':
        { _id: '6092ba6bd758430012abd938',
           id: '1',
           x: 29,
           y: 9,
           canvas: '0',
           width: 10,
           height: 3,
           type: 'ATTRIBUTE_DISPLAY',
           inputs: {
                attribute: {
                    device: "test/webjivetestdevice/1",
                    attribute: "state",
                    label: "State"
                },
              precision: 2,
              showDevice: false,
              showAttribute: 'Label',
              scientificNotation: false,
              showEnumLabels: false,
              textColor: '#000000',
              backgroundColor: '#ffffff',
              size: 1,
              font: 'Helvetica' },
           order: 0,
           valid: true
        }
    }
    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    const SageStep4put = step([DASHEDITEXAMPLE]);
    //check if widgets are ordered by id
    expect(SageStep4put.payload.action.dashboard.widgets).toEqual(reorderedWigets);
});

test('the editWidget RESIZE_WIDGET happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: RESIZE_WIDGET,
        dx: 0, 
        dy: 0, 
        mx: 1, 
        my: 1, 
        id: "1",
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    
    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});

test('the editWidget DUPLICATE_WIDGET happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: DUPLICATE_WIDGET,
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    
    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});

test('the editWidget DELETE_WIDGET happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const payloadExample = {
        type: DELETE_WIDGET,
    };

    const SageStep2call = step(payloadExample);
    expect(SageStep2call.type).toEqual('SELECT');// select(getSelectedDashboard);

    const SageStep3put = step(DASHEDITEXAMPLE);
    expect(SageStep3put.type).toEqual('SELECT');// select(getDashboards);

    
    const SageStep4put = step([DASHEDITEXAMPLE]);
    expect(SageStep4put.type).toEqual('PUT');
    expect(SageStep4put.payload.action.type).toEqual(DASHBOARD_EDITED);
    expect(SageStep4put.payload.action.dashboard.id).toEqual('6092b53fd758430012abd91e');

    const SageStep5put = step(DASHEDITEXAMPLE);
    expect(SageStep5put.type).toEqual('PUT');
    expect(SageStep5put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep5put.payload.action.id).toEqual('6092b53fd758430012abd91e');
    
});


test('the editWidget WIDGET_CLIPBOARD_PASTE happy path', async () => {

    const step = stepper(editWidget());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual(editWidgetActions);

    const exampleClipboardWidgets =
        [
            {
                "id": "6092b53fd758430012abd91e",
                "x": 30,
                "y": 10,
                "canvas": "0",
                "width": 10,
                "height": 3,
                "type": "ATTRIBUTE_DISPLAY",
                "inputs": {
                    "attribute": {
                        "device": "test/webjivetestdevice/1",
                        "attribute": "state",
                        "label": "State"
                    },
                    "precision": 2,
                    "showDevice": false,
                    "showAttribute": "Label",
                    "scientificNotation": false,
                    "showEnumLabels": false,
                    "textColor": "#000000",
                    "backgroundColor": "#ffffff",
                    "size": 1,
                    "font": "Helvetica"
                },
                "order": 0,
                "valid": true
            }
        ]
        ;

    const SageStep2call = step({ type: WIDGET_CLIPBOARD_PASTE });
    expect(SageStep2call.type).toEqual('SELECT');

    const SageStep3call = step(DASHEDITEXAMPLE);
    expect(SageStep3call.type).toEqual('SELECT');

    const SageStep4call = step(exampleClipboardWidgets);
    expect(SageStep4call.type).toEqual('SELECT');

    const SageStep5call = step(exampleClipboardWidgets);
    expect(SageStep5call.type).toEqual('SELECT');

    const SageStep6call = step(DASHEDITEXAMPLE);
    expect(SageStep6call.type).toEqual('PUT');
    expect(SageStep6call.payload.action.type).toEqual(DASHBOARD_EDITED);

    const SageStep6put = step(DASHEDITEXAMPLE);
    expect(SageStep6put.type).toEqual('PUT');
    expect(SageStep6put.payload.action.type).toEqual(SAVE_DASHBOARD);
    expect(SageStep6put.payload.action.id).toEqual('6092b53fd758430012abd91e');
});

test('the notifyOnSave happy path', async () => {

    const step = stepper(notifyOnSave());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: DASHBOARD_SAVED });
    
    const SageStep2call = step({ created: true });
    expect(SageStep2call.type).toEqual('PUT');
    expect(SageStep2call.payload.action.notification.action).toEqual(DASHBOARD_CREATED);
});

test('the notifyOnShare happy path', async () => {

    const step = stepper(notifyOnShare());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: DASHBOARD_SHARED });
    
    const SageStep2call = step({ group: 'Group1', groupWriteAccess: true });
    expect(SageStep2call.type).toEqual('PUT');
    expect(SageStep2call.payload.action.notification.action).toEqual(DASHBOARD_SHARED);
});

test('the notifyOnClone happy path', async () => {

    const step = stepper(notifyOnClone());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: DASHBOARD_CLONED });
    
    const SageStep2call = step();
    expect(SageStep2call.type).toEqual('PUT');
    expect(SageStep2call.payload.action.notification.action).toEqual(DASHBOARD_CLONED);
});

test('the notifyOnDelete happy path', async () => {

    const step = stepper(notifyOnDelete());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: DASHBOARD_DELETED });
    
    const SageStep2call = step();
    expect(SageStep2call.type).toEqual('PUT');
    expect(SageStep2call.payload.action.notification.action).toEqual(DASHBOARD_DELETED);
});

test('the notifyOnExport happy path', async () => {

    const step = stepper(notifyOnExport());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: DASHBOARD_EXPORTED });
    
    const SageStep2call = step({dashboard: DASHEDITEXAMPLE});
    expect(SageStep2call.type).toEqual('PUT');
    expect(SageStep2call.payload.action.notification.action).toEqual(DASHBOARD_EXPORTED);
});

test('the hideNotificationAfterDelay happy path', async () => {

    const step = stepper(hideNotificationAfterDelay());

    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('TAKE');
    expect(SageStep1take.payload).toEqual({pattern: SHOW_NOTIFICATION });
    
    const SageStep2call = step({notification: "Hello" });
    expect(SageStep2call.type).toEqual('RACE');
    expect(SageStep2call.payload.newNotification).not.toBe(undefined);
    expect(SageStep2call.payload.timePassed).not.toBe(undefined);

    const SageStep3call = step({timePassed: true});
    expect(SageStep3call.type).toEqual('PUT');
    expect(SageStep3call.payload.action.type).toBe(HIDE_NOTIFICATION);
});
